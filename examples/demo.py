from stratipy import utils, sentinel2, elevation, soils
from shapely.geometry import shape
import matplotlib.pyplot as plt

# Load data
fields = utils.load_fields_from_projects_file(
    "../../data/esmc-boundaries-anonymized-11-12-2020.json"
)

# Generate boundary for stratification
boundary = shape(fields[0]["boundaryGeoJSON"])
bbox = boundary.bounds


# Use Sentinel 2 data to calculate NDVI summary over 2020 for AOI
greenest = sentinel2.get_ndvi_summary_by_bbox(bbox)
plt.title("Year NDVI Summary")
plt.imshow(greenest)

# This is equivalent to:
# import re
# catalog = sentinel2.get_s2_catalog(bbox, dates="2020-01-01/2020-12-31")
# ins, wins = sentinel2.get_inputs(
#     {key: catalog[key] for key in list(catalog)[:] if re.match(r'^S2A.*$', key)},
# #     catalog,
#     bbox
# )
# rasters_by_date, bad_rasters_by_date = sentinel2.fetch_async(ins, wins)
# greenest = sentinel2.summarize_ndvi(rasters_by_date)

# Use 3DEP data to calculate slope and aspect for AOI
slope, aspect = elevation.calculate_slope_aspect(elevation.fetch_elevation(bbox))
plt.figure()
plt.title("Slope")
plt.imshow(slope)
plt.figure()
plt.title("Aspect")
plt.imshow(aspect)
