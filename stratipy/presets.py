from stratipy import constants

INFINITY = 9.999999999e99

carbofarm_sampling_densities = [
  { 
    'min_area': 0, 
    'max_area': 5, 
    'units': 'HECTARES', 
    'density': 2, 
    'per': 'STRATUM' 
  },
  { 
    'min_area': 5, 
    'max_area': 15, 
    'units': 'HECTARES', 
    'density': 1,  
    'per': 'STRATUM' 
  },
]

esmc_sampling_densities = [
    { 
    'min_area': 0, 
    'max_area': INFINITY, 
    'units': 'HECTARES', 
    'density': (1/4) / constants.HECTARE_PER_ACRE, # 4 acres/sample  per acre === .61776 samples per hectare
    'per': 'STRATUM' 
  }
]

sk_sampling_densities = [
  { 
    'min_area': 0, 
    'max_area': 5, 
    'units': 'HECTARES', 
    'density': 2, 
    'min_samplings': 3,
    'per': 'STRATUM' 
  },
  { 
    'min_area': 5, 
    'max_area': 15, 
    'units': 'HECTARES', 
    'density': 1,
    'min_samplings': 3,  
    'per': 'STRATUM' 
  },
]

carbofarm_sampling_densities_legacy = [
    { 
    'min_area': 0, 
    'max_area': INFINITY, 
    'units': 'HECTARES', 
    'density': 2, 
    'per': 'STRATUM' 
  }
]

esmc_sampling_densities_legacy = [
    {
    'min_area': 0, 
    'max_area': INFINITY,
    'units': 'HECTARES', 
    'density': (27/80) / constants.HECTARE_PER_ACRE, # 27/80 acres/sample  per acre === 0.8353 samples per hectare
    'per': 'STRATUM' 
    }
]

