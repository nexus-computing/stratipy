from math import ceil

def get_sampling_density(stratum_area, densities):
    '''
calculate sampling density with stratipy presents and stratum area

Parameters:
    stratum_area: array, required
        field to be stratified
    densities: tuple, required
        sampling density from stratipy presets

    '''

    for density in densities:
        if stratum_area > density['min_area'] and stratum_area <= density['max_area']:
            return density['density']
    
    raise ValueError('invalid stratum area')


def get_number_of_samples(stratum_area, densities):
    '''
calculate number of samples for CLHS with stratipy presents and stratum area

Parameters:
    stratum_area: array, required
        field to be stratified
    densities: tuple, required
        sampling density from stratipy presets

    '''
        
    for density in densities:
        if stratum_area > density['min_area'] and stratum_area <= density['max_area']:
            sampling_min = density['min_samplings'] if 'min_samplings' in density else 1
            return max(sampling_min, ceil((stratum_area*density['density'])))
        
    raise ValueError('invalid stratum area')


