import pytest
from stratipy import stratify, presets, constants

def test_get_sampling_density_carbofarm():
    assert stratify.get_sampling_density(3, presets.carbofarm_sampling_densities) == 2
    assert stratify.get_sampling_density(7, presets.carbofarm_sampling_densities) == 1
    assert stratify.get_sampling_density(5, presets.carbofarm_sampling_densities) == 2
    with pytest.raises(ValueError):
        stratify.get_sampling_density(16, presets.carbofarm_sampling_densities) 
    with pytest.raises(ValueError):
        stratify.get_sampling_density(0, presets.carbofarm_sampling_densities)
    
def test_get_sampling_density_ESMC():
    assert stratify.get_sampling_density(3, presets.esmc_sampling_densities) == (1/4) / constants.HECTARE_PER_ACRE
    assert stratify.get_sampling_density(100000, presets.esmc_sampling_densities) == (1/4) / constants.HECTARE_PER_ACRE

def test_get_number_of_samples_sk():
    assert stratify.get_number_of_samples(3, presets.sk_sampling_densities) == 6
    assert stratify.get_number_of_samples(7, presets.sk_sampling_densities) == 7
    assert stratify.get_number_of_samples(.5, presets.sk_sampling_densities) == 3
    with pytest.raises(ValueError):
        stratify.get_number_of_samples(16, presets.carbofarm_sampling_densities)
    with pytest.raises(ValueError):
        stratify.get_number_of_samples(0, presets.carbofarm_sampling_densities)

def test_get_number_of_samples_carbofarm_without_min_sample_number():
    assert stratify.get_number_of_samples(1, presets.carbofarm_sampling_densities) == 2
