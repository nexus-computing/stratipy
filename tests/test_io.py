import pytest
from stratipy import io
import json
import time_machine
import datetime as dt

MOCK_DATE_STRING = '1970-01-01T00:00:00Z'
MOCK_DATE = dt.datetime(1970, 1, 1)

@pytest.fixture
def field_boundary():
	return {
		'type': 'Polygon',
		'coordinates': [
			[
				[-84.59305286407469, 39.556934508617076],
				[-84.59270954132079, 39.55203740161884],
				[-84.58326816558838, 39.55213667073393],
				[-84.58386898040771, 39.55607423106763],
				[-84.58438396453857, 39.555644088291956],
				[-84.58524227142334, 39.555644088291956],
				[-84.58549976348876, 39.55676907145539],
				[-84.59305286407469, 39.556934508617076]
			]
		]
	}

@pytest.fixture
def location_coords():
	return [
		(39.55519893168147, -84.58548552017902),
		(39.55246191748659, -84.58383482805323),
		(39.55437420270519, -84.58481585750525),
		(39.5548436670673, -84.5857306022019),
		(39.55547142333218, -84.58559244806948),
		(39.55418066782743, -84.59285266811011),
		(39.55484540868369, -84.59015295498435),
		(39.55260996244743, -84.5866227017214),
		(39.55645927829489, -84.58544179916504),
		(39.55452222646758, -84.58760381087788),
		(39.556200610161966, -84.59033879815297),
		(39.55305455591696, -84.59068045868192),
		(39.55586776916871, -84.58732441446747),
		(39.55334874655486, -84.5918340447357),
		(39.5541772243936, -84.58400804523929),
	]

def test_create_export_file(location_coords, field_boundary):
	mock_agent = 'My Agent'
	mock_args = { 
		'sample_density': [
			{ 
				'min': 0, 
				'max': 5, 
				'units': 'HECTARES', 
				'density': 2, 
				'per': 'STRATUM' 
			}
		],
		'agent': mock_agent,
	}
	actual_string = io.create_export_file(
		location_coords, 
		field_boundary,
		stratification_args=mock_args
	)
	actual = json.loads(actual_string)
	assert len(actual['locationCollections']) == 1
	assert len(actual['areas']) == 1
	assert len(actual['stratifications']) == 1
	assert len(actual['fields']) == 1
	assert len(actual['locations']) == len(location_coords)
	area = actual['areas'][0]
	field = actual['fields'][0]
	stratification = actual['stratifications'][0]
	location_collection = actual['locationCollections'][0]
	locations = actual['locations']

	assert field['area'][0] == area['id']
	assert stratification['object'] == area['id']
	assert stratification['agent'] == mock_agent
	stratification_input_args = {x['name']: x['value'] for x in stratification['input']}
	assert stratification_input_args['SAMPLE_DENSITY'] == [
		{ 
			'min': 0, 
			'max': 5, 
			'units': 'HECTARES', 
			'density': 2, 
			'per': 'STRATUM' 
		}
	]
	assert location_collection['resultOf'] == stratification['id']
	assert location_collection['object'] == area['id']
	assert location_collection['features'] == [loc['id'] for loc in locations]
	assert location_collection['featureOfInterest'] == field['id']
	
@time_machine.travel(dt.datetime(1970, 1, 1))
def test_create_area(field_boundary):
	field_id = 'field_1'
	creator = 'bob'
	name = 'Field 1'
	area = io.create_area(field_boundary, field_id=field_id, creator=creator, name=name)
	assert area['id']
	assert area['geometry'] == field_boundary
	assert type(area['properties']) == dict
	assert area['properties']['name'] == name
	assert area['properties']['featureOfInterest'] == field_id
	assert area['properties']['dateCreated'] == MOCK_DATE_STRING
	assert area['properties']['creator'] == creator
	assert area['type'] == 'Feature'

@time_machine.travel(MOCK_DATE)
def test_create_stratification():
	area_id = 'area_1'
	name = 'My Stratification'
	agent = 'My Agent'
	clhs_max_iterations = 1000
	sample_density = [
		{ 
			'min': 0, 
			'max': 5, 
			'units': 'HECTARES', 
			'density': 2, 
			'per': 'STRATUM' 
		}
  	]
	number_of_clusters = 5
	random_seed = 10
	algorithm_version = '0.2.0'
	boundary_buffer_meters = -30
	s2_masked_ratio_threshold = 0.3
	s2_ndvi_max = 1.0
	s2_ndvi_date_range = "2021-01-01/2021-12-31"

	stratification = io.create_stratification(
		area_id=area_id,
		name=name,
		algorithm_version=algorithm_version,
		agent=agent,
		clhs_max_iterations=clhs_max_iterations,
		number_of_clusters=number_of_clusters,
		sample_density=sample_density,
		random_seed=random_seed,
		boundary_buffer_meters=boundary_buffer_meters,
		s2_masked_ratio_threshold = s2_masked_ratio_threshold,
		s2_ndvi_date_range = s2_ndvi_date_range
	)
	assert stratification['object'] == area_id
	assert stratification['name'] == name
	assert stratification['dateCreated'] == MOCK_DATE_STRING
	assert type(stratification['algorithm']) == dict
	assert stratification['algorithm']['name'] == 'OUR_SCI_STRATIFICATION'
	assert stratification['algorithm']['alternateName'] == 'SKLEARN_KMEANS_RANGE_CLHS'
	assert stratification['algorithm']['version'] == algorithm_version
	assert stratification['dateCreated'] == MOCK_DATE_STRING
	assert stratification['agent'] == agent
	input_args = {x['name']: x['value'] for x in stratification['input']}
	assert input_args['INPUT_LAYER_SENTINEL_2_L2A']
	assert input_args['INPUT_LAYER_3DEP']
	assert input_args['INPUT_LAYER_SSURGO']
	assert input_args['RANDOM_SEED'] == random_seed
	assert input_args['CLHS_MAX_ITERATIONS'] == clhs_max_iterations
	assert input_args['CLUSTERING_NUMBER_OF_CLUSTERS'] == number_of_clusters
	assert input_args['SAMPLE_DENSITY'] == sample_density
	assert input_args['FIELD_BOUNDARY_RASTER_MASK_BUFFER_METERS'] == boundary_buffer_meters
	assert input_args['INPUT_LAYER_SENTINEL_2_L2A_SCL_MASKED_RATIO_THRESHOLD'] == s2_masked_ratio_threshold
	assert input_args['INPUT_LAYER_SENTINEL_2_L2A_NDVI_MAX_VALUE'] == s2_ndvi_max
	assert input_args['INPUT_LAYER_SENTINEL_2_L2A_NDVI_DATE_RANGE'] == s2_ndvi_date_range

@time_machine.travel(MOCK_DATE)
def test_create_stratification_returns_default_ndvi_max_value():
	stratification = io.create_stratification()
	input_args = {x['name']: x['value'] for x in stratification['input']}
	assert input_args['INPUT_LAYER_SENTINEL_2_L2A_NDVI_MAX_VALUE'] == 1.0

@time_machine.travel(MOCK_DATE)
def test_create_stratification_returns_clhs_boundary_buffer():
	stratification = io.create_stratification(clhs_buffer_meters=-10)
	input_args = {x['name']: x['value'] for x in stratification['input']}
	assert input_args['BOUNDARY_CLHS_RASTER_MASK_BUFFER_METERS'] == -10

@time_machine.travel(MOCK_DATE)
def test_create_stratification_does_not_include_clhs_boundary_buffer_when_absent():
	stratification = io.create_stratification()
	input_args = {x['name']: x['value'] for x in stratification['input']}
	assert 'BOUNDARY_CLHS_RASTER_MASK_BUFFER_METERS' not in input_args


@time_machine.travel(MOCK_DATE)
def test_create_stratification_with_no_custom_input_layers():
	area_id = 'area_1'
	name = 'My Stratification'
	agent = 'My Agent'
	clhs_max_iterations = 1000
	sample_density = []
	number_of_clusters = 5
	random_seed = 10
	algorithm_version = '0.2.0'
	boundary_buffer_meters = -30

	stratification = io.create_stratification(
		area_id=area_id,
		name=name,
		algorithm_version=algorithm_version,
		agent=agent,
		clhs_max_iterations=clhs_max_iterations,
		number_of_clusters=number_of_clusters,
		sample_density=sample_density,
		random_seed=random_seed,
		boundary_buffer_meters=boundary_buffer_meters,
		input_layers=[],
	)

	input_args = {x['name']: x['value'] for x in stratification['input']}
	assert 'INPUT_LAYER_SENTINEL_2_L2A' not in input_args
	assert 'INPUT_LAYER_3DEP' not in input_args
	assert 'INPUT_LAYER_SSURGO' not in input_args
	assert 'INPUT_LAYER_COPERNICUS_GLO_30' not in input_args
	assert 'RANDOM_SEED' in input_args
	assert 'CLHS_MAX_ITERATIONS' in input_args
	assert 'CLUSTERING_NUMBER_OF_CLUSTERS' in input_args
	assert 'SAMPLE_DENSITY' in input_args
	assert 'FIELD_BOUNDARY_RASTER_MASK_BUFFER_METERS' in input_args
	assert len(input_args) == 5

def test_create_stratification_layer_dependent_params_without_layer():
	area_id = 'area_1'
	name = 'My Stratification'
	agent = 'My Agent'
	clhs_max_iterations = 1000
	sample_density = []
	number_of_clusters = 5
	random_seed = 10
	algorithm_version = '0.2.0'
	boundary_buffer_meters = -30
	s2_ndvi_max = 1.0
	s2_masked_ratio_threshold = 0.3
	s2_ndvi_date_range = "2021-01-01/2021-12-31"

	stratification = io.create_stratification(
		area_id=area_id,
		name=name,
		algorithm_version=algorithm_version,
		agent=agent,
		clhs_max_iterations=clhs_max_iterations,
		number_of_clusters=number_of_clusters,
		sample_density=sample_density,
		random_seed=random_seed,
		boundary_buffer_meters=boundary_buffer_meters,
		s2_masked_ratio_threshold = s2_masked_ratio_threshold,
		s2_ndvi_max = s2_ndvi_max,
		s2_ndvi_date_range = s2_ndvi_date_range,
		input_layers=[],
	)

	input_args = {x['name']: x['value'] for x in stratification['input']}
	assert 'INPUT_LAYER_SENTINEL_2_L2A' not in input_args
	assert 'INPUT_LAYER_3DEP' not in input_args
	assert 'INPUT_LAYER_SSURGO' not in input_args
	assert 'INPUT_LAYER_SENTINEL_2_L2A_SCL_MASKED_RATIO_THRESHOLD' not in input_args
	assert 'INPUT_LAYER_SENTINEL_2_L2A_NDVI_MAX_VALUE' not in input_args
	assert 'INPUT_LAYER_SENTINEL_2_L2A_NDVI_DATE_RANGE' not in input_args

@time_machine.travel(MOCK_DATE)
def test_create_stratification_with_custom_layers():
	area_id = 'area_1'
	name = 'My Stratification'
	agent = 'My Agent'
	clhs_max_iterations = 1000
	sample_density = []
	number_of_clusters = 5
	random_seed = 10
	algorithm_version = '0.2.0'
	boundary_buffer_meters = -30
	s2_masked_ratio_threshold = 0.3
	s2_ndvi_date_range = "2021-01-01/2021-12-31"

	stratification = io.create_stratification(
		area_id=area_id,
		name=name,
		algorithm_version=algorithm_version,
		agent=agent,
		clhs_max_iterations=clhs_max_iterations,
		number_of_clusters=number_of_clusters,
		sample_density=sample_density,
		random_seed=random_seed,
		boundary_buffer_meters=boundary_buffer_meters,
		s2_masked_ratio_threshold = s2_masked_ratio_threshold,
		s2_ndvi_date_range = s2_ndvi_date_range,
		input_layers=['INPUT_LAYER_COPERNICUS_GLO_30', 'INPUT_LAYER_SENTINEL_2_L2A'],
	)

	input_args = {x['name']: x['value'] for x in stratification['input']}
	assert 'INPUT_LAYER_SENTINEL_2_L2A' in input_args
	assert 'INPUT_LAYER_COPERNICUS_GLO_30' in input_args
	assert 'INPUT_LAYER_3DEP' not in input_args
	assert 'INPUT_LAYER_SSURGO' not in input_args
	assert 'RANDOM_SEED' in input_args
	assert 'CLHS_MAX_ITERATIONS' in input_args
	assert 'CLUSTERING_NUMBER_OF_CLUSTERS' in input_args
	assert 'SAMPLE_DENSITY' in input_args
	assert 'FIELD_BOUNDARY_RASTER_MASK_BUFFER_METERS' in input_args
	assert len(input_args) == 10

@time_machine.travel(MOCK_DATE)
def test_create_stratification_raises_when_passed_bad_input_layers():
	with pytest.raises(Exception):
		io.create_stratification(input_layers=['asdf'])

# def test_create_location_collection():

def test_create_location(location_coords):
	point_coords = location_coords[0]
	actual = io.create_location(
		point_coords,
	)
	assert actual['geometry']['coordinates'] == point_coords
	
def test_create_location_with_stratum_label_composite_bulk_density_required_numbers(location_coords):
	point_coords = location_coords[0]
	actual = io.create_location(
		point_coords,
		stratum=5,
		label=5,
		composite=5,
		bulk_density_required=True,
	)
	assert actual['geometry']['coordinates'] == point_coords
	assert actual['properties']['stratum'] == '5'
	assert actual['properties']['composite'] == '5'
	assert actual['properties']['label'] == '5'
	assert actual['properties']['bulkDensityRequired'] == True


def test_create_location_with_stratum_label_composite_bulk_density_required_strings(location_coords):
	point_coords = location_coords[0]
	actual = io.create_location(
		point_coords,
		stratum='5',
		label='5',
		composite='5',
		bulk_density_required=True,
	)
	assert actual['geometry']['coordinates'] == point_coords
	assert actual['properties']['stratum'] == '5'
	assert actual['properties']['composite'] == '5'
	assert actual['properties']['label'] == '5'
	assert actual['properties']['bulkDensityRequired'] == True

# def test_create_field():


def test_create_stratification_export(location_coords):
	mock_area_id = 'mock_area_id'
	mock_field_id = 'mock_field_id'
	mock_agent = 'My Agent'
	mock_args = { 
		'sample_density': 555,
		'agent': mock_agent,
	}

	actual_string = io.create_stratification_export(
		location_coords, 
		area_id=mock_area_id,
		field_id=mock_field_id,
		stratification_args=mock_args
	)
	actual = json.loads(actual_string)
	
	stratification = actual['stratification']
	location_collection = actual['locationCollection']

	assert not 'id' in stratification
	assert stratification['object'] == mock_area_id
	assert stratification['agent'] == mock_agent
	assert not 'id' in location_collection
	assert not 'resultOf' in location_collection
	assert location_collection['featureOfInterest'] == mock_field_id
	assert len(location_collection['features']) == len(location_coords)
	
	for i, loc in enumerate(location_collection['features']):
		assert not 'id' in loc
		assert loc['properties']['label'] == str(i)
		assert loc['type'] == 'Feature'
		assert loc['geometry']['type'] == 'Point'
		assert tuple(loc['geometry']['coordinates']) == location_coords[i]

def test_create_stratification_export_with_location_strata(location_coords):
	mock_area_id = 'mock_area_id'
	mock_field_id = 'mock_field_id'
	mock_agent = 'My Agent'
	mock_args = { 
		'sample_density': 555,
		'agent': mock_agent,
	}
	mock_location_strata = range(len(location_coords))

	actual_string = io.create_stratification_export(
		location_coords, 
		area_id=mock_area_id,
		field_id=mock_field_id,
		stratification_args=mock_args,
		location_strata=mock_location_strata
	)
	actual = json.loads(actual_string)
	
	location_collection = actual['locationCollection']
	
	for i, loc in enumerate(location_collection['features']):
		assert loc['properties']['stratum'] == str(i)
		assert not 'id' in loc
		assert loc['type'] == 'Feature'
		assert loc['geometry']['type'] == 'Point'
		assert tuple(loc['geometry']['coordinates']) == location_coords[i]

